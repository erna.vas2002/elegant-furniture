import axios from 'axios'

export default {
  GET_FURNITURE_TO_API({ commit }) {
    return axios('http://localhost:3000/furniture',
      {
        methods: "GET"
      })
      .then((furniture) => {
        commit("SET_FURNITURE_TO_STATE", furniture.data)
      })
      .catch((error) => {
        console.log(error)
      })
  },
  ADD_TO_CART({commit}, product) {
    commit("SET_CART", product)
  },
  DELETE_TO_CART({commit}, index) {
    commit("DELETE_PRODUCT", index)
  },
  INCREMENT_PRODUCT({commit}, index) {
    commit("INCREMENT", index)
  },
  DECREMENT_PRODUCT({commit}, index) {
    commit("DECREMENT", index)
  }
}