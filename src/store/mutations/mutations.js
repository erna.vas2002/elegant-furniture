export default {
  SET_FURNITURE_TO_STATE: (state, furniture) => {
    state.furniture = furniture
  },
  SET_CART: (state, product) => {
    if (state.cart.length) {
      let isThingExists = false
      state.cart.map(item => {
        if (item.article === product.article) {
          isThingExists = true
          item[index].quantity++
        }
      })
      if (!isThingExists) {
        state.cart.push(product)
      }
    } else {
      state.cart.push(product)
    }
  },
  DELETE_PRODUCT: (state, index) => {
    state.cart.splice(index, 1)
  },
  INCREMENT: (state, index) => {
    state.cart[index].quantity++
  },
  DECREMENT: (state, index) => {
    if (state.cart[index].quantity > 1) {
      state.cart[index].quantity--
    }
  },
}