import Vue from 'vue'
import Vuex from 'vuex'

import getters from './getters/getters'
import actions from './actions/actions'
import mutations from './mutations/mutations'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    furniture: [],
    cart: []
  },
  getters,
  mutations,
  actions,
  modules: {
  }
})
