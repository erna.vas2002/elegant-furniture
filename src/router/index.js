import Vue from 'vue'
import VueRouter from 'vue-router'
import ProductsView from '../views/ProductsView.vue'

import Catalog from '../components/catalog/Catalog.vue'
import Cart from '../components/cart/Cart.vue'

import Registration from '../components/form/Registration.vue'
import Entrance from '../components/form/Entrance.vue'

Vue.use(VueRouter)

const routes = [
  // {
  //   path: '/',
  //   name: 'home',
  //   component: HomeView
  // },
  {
    path: '/',
    name: 'products',
    component: ProductsView
  },
  // {
  //   path: '/contacts',
  //   name: 'contacts',
  //   component: ContactsView
  // },
  {
    path: '/catalog',
    name: 'catalog',
    component: Catalog
  },
  {
    path: '/cart',
    name: 'cart',
    component: Cart
  },
  {
    path: '/registration',
    name: 'registration',
    component: Registration
  },
  {
    path: '/entrance',
    name: 'entrance',
    component: Entrance
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
